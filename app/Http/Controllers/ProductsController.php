<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Auth::check()) {
            $id = Auth::id();
            $user = User::Find($id);
            $products = products::All();
            return view('products.index', ['products' => $products]);    
         return redirect()->intended('/home');
    }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        {
        
            if (Auth::check()) {
            return view('products.create');
            }
            return redirect()->intended('/home');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (Auth::check()) {
            $products = new Task(); 
        $products->title = $request->title;
        $products->status = $request->status;
        $products->user_id = Auth::id();
        $products->created_at = null;
        $products->updated_at = null;
        $products->save();
        return redirect('products');
            }
            return redirect()->intended('/home');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (Auth::check()) {
            return redirect()->intended('/products');
            }
            return redirect()->intended('/home');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (Auth::check()) {
             
            $products = products::find($id);
                return view('products.edit', compact('products'));

            }
            return redirect()->intended('/home');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (Auth::check()) {
            $products = products::findOrFail($id);
       
        $products-> update($request->all());
     
        
            return redirect('products');           
        }
            
            return redirect()->intended('/home');
    }
    

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
        
 
        if (Auth::check()) {
            if (Gate::denies('owner')) {
                abort(403,"Sorry you are not allowed to delete or edit  products..");
            }
            $products = products::find($id);
            $products->delete();
            return redirect('products');
            
            }
            return redirect()->intended('/home');
    
}
}